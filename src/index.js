import logger from './logger';
import ParsedCSV from './parsedCsv';
import GeoSearch from './geoSearch';
import ExpressServer from './expressServer';
import defaultConfig from '../config.json';

const parser = new ParsedCSV();
const start = async () => {
  logger.debug(`Starting with default config: ${JSON.stringify(defaultConfig)}`);
  const data = await parser.parse();
  const geoSearch = new GeoSearch(data);
  const expressServer = new ExpressServer(geoSearch);
  return expressServer.listen();
};

start()
  .then(() => {
    logger.debug('Server Started');
  })
  .catch((e) => {
    logger.error(e);
  });
