import winston from 'winston';
import defaultOptions from '../config.json';

winston.configure({
  transports: [
    new winston.transports.Console({
      json: false,
      colorize: true,
      level: defaultOptions.logLevel,
    }),
  ],
});

export default winston;
