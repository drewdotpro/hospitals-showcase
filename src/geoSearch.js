import Geo from 'geo-nearby';
import _ from 'lodash';
import logger from './logger';
import defaultOptions from '../config.json';

class GeoSearch {
  constructor(data, options = {}) {
    this.options = _.defaultsDeep({}, options, defaultOptions);
    if (!data) {
      throw new Error('No data provided');
    }
    this.data = data;
    const compactSetData = this.data.map((hospital, idx) =>
      [parseFloat(hospital.Latitude), parseFloat(hospital.Longitude), idx]);
    const dataSet = Geo.createCompactSet(compactSetData);
    this.geo = new Geo(dataSet, { sorted: true, limit: this.options.searchLimit });
    logger.debug('GeoSearch created');
  }

  nearest(lat, lon) {
    const results = this.geo.nearBy(
      parseFloat(lat),
      parseFloat(lon),
      [0, this.options.searchRadius],
    );
    logger.debug(`GeoSearch for ${lat}, ${lon}`, results);
    return results.map(({ i }) => {
      const {
        OrganisationID, OrganisationCode, OrganisationName, Latitude, Longitude,
      } = this.data[i];
      return {
        organisationID: OrganisationID,
        organisationCode: OrganisationCode,
        organisationName: OrganisationName,
        Latitude: parseFloat(Latitude).toString(),
        Longitude: parseFloat(Longitude).toString(),
      };
    });
  }
}

export default GeoSearch;
