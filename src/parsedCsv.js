import csvParse from 'csv-parse';
import fs from 'fs';
import { promisify } from 'util';
import _ from 'lodash';
import defaultOptions from '../config.json';

const parse = promisify(csvParse);
const readFile = promisify(fs.readFile);

class ParsedCSV {
  constructor(options = {}) {
    this.options = _.defaultsDeep({}, options, defaultOptions);
  }

  async parse() {
    this.stringData = await readFile(this.options.file, this.options.encoding);
    this.parsedData = await parse(this.stringData, this.options.csvOptions);
    return this.parsedData;
  }
}

export default ParsedCSV;
