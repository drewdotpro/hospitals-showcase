import express from 'express';
import helmet from 'helmet';
import compression from 'compression';
import winston from 'winston';
import expressWinston from 'express-winston';
import _ from 'lodash';
import assert from 'assert';
import logger from './logger';
import defaultOptions from '../config.json';


class ExpressServer {
  constructor(geoSearch, options = {}) {
    this.options = _.defaultsDeep({}, options, defaultOptions);
    this.geoSearch = geoSearch;
    this.app = express();
    this.app.use(expressWinston.logger({
      transports: [
        new winston.transports.Console({
          json: false,
          colorize: true,
          level: this.options.logLevel,
        }),
      ],
      meta: true,
      msg: 'HTTP {{req.method}} {{req.url}}',
      expressFormat: true,
      colorize: true,
    }));
    this.app.use(helmet());
    this.app.use(compression());
    this.app.get('/nearest', this.nearest.bind(this));
    this.errorHandler();
    logger.debug('ExpressServer created');
  }

  listen() {
    return new Promise((resolve, reject) => {
      try {
        this.app.listen(this.options.port, () => {
          logger.info(`Listening on port ${this.options.port}`);
          resolve();
        });
      } catch (e) {
        reject(e);
      }
    });
  }

  nearest(req, res, next) {
    const { lat, lon } = req.query;
    logger.debug(`Recevied request for nearest with lat '${lat}' and lon '${lon}'`);
    try {
      assert(lat, 'You must provided the parameter `lat`');
      assert(lon, 'You must provided the parameter `lon`');
      assert(!Number.isNaN(parseFloat(lat)), 'lat is NaN');
      assert(!Number.isNaN(parseFloat(lon)), 'lon is NaN');
    } catch (e) {
      e.status = 422;
      return next(e);
    }
    try {
      const results = this.geoSearch.nearest(lat, lon);
      return res.json(results);
    } catch (e) {
      return next(e);
    }
  }

  errorHandler() {
    const handler = (err, req, res, next) => {
      if (res.headersSent) {
        return next(err);
      }
      const status = err.status || 500;
      res.status(status);
      let errorMessage = 'A server error occured';
      if (status >= 400 && status < 500) {
        errorMessage = err.message;
        logger.debug(err);
      } else {
        logger.error(err);
      }
      const result = {
        error: errorMessage,
      };
      return res.json(result);
    };
    this.app.use(handler);
  }
}

export default ExpressServer;
