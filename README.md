# NodeJS Code Challenge

## Repository Structure
`data/` contains the `Hospital.csv` file provided for the test.

`lib/` contains the application compiled to NodeJS v8.9.4 LTS by Babel.

`src/` contains the source files of the application.

`config.json` is the configuration file for the project.

## Setup

To set up the repository simply run
```
npm i
```
If you are running a different version of NodeJS to `v8.9.4` it is advised to rebuild the `lib/` folder:
```
npm run build
```

## Commands

### Build

Builds `src/` folder to lib folder for current node version.
```
npm run build
```

### Start

To run the compiled code as a server on port 3000:
```
npm run start
```

### Dev

To run the application in 'watch for changes mode':
```
npm run dev
```

### Lint

To validate the codebase against the eslint requirements:
```
npm run lint
```

## Configuration

The config.json file defines the configurable options for the project.

`file`: the path to the csv file containing hospitals.

`encoding`: the encoding of the file.

`csvOptions`: the options passed to csv parser, see http://csv.adaltas.com/parse/ for more information.

`searchRadius`: the radius in metres to search from the given point. Current value covers Earth.

`searchLimit`: maximum number of results to return .

`port`: port to launch the server on.

`logLevel`: level of logs to output to the console.

## Package Review
All chosen packages pass Node Security Project checks.

To run checks yourself:
```
npm i -g nsp
nsp check
```

## Usage
After (optionally) running `npm run build` and `npm run start` the console will inform you when the server is listening:
```
info: Listening on port 3000
debug: Server Started

```
From here you can cURL or request the data in your browser, for example, requesting:
```
http://localhost:3000/nearest?lat=53.85313416&lon=-0.411472321
```
Will give the return data:
```
[{"organisationID":"1421","organisationCode":"RV9HE","organisationName":"East Riding Community Hospital","Latitude":"53.85313416","Longitude":"-0.411472321"},{"organisationID":"42260","organisationCode":"RV941","organisationName":"Hawthorne Court - Inpatient Unit","Latitude":"53.84577942","Longitude":"-0.434020907"},{"organisationID":"2027176","organisationCode":"RV9FH","organisationName":"Humber Centre for Forensic Psychiatry","Latitude":"53.7702446","Longitude":"-0.44977659"},{"organisationID":"42661","organisationCode":"RWA16","organisationName":"Castle Hill Hospital","Latitude":"53.7767334","Longitude":"-0.444969475"},{"organisationID":"70616","organisationCode":"RV942","organisationName":"Mill View Court - Inpatient Unit","Latitude":"53.7767334","Longitude":"-0.444969475"}]
```

Malformed requests will receive a 422 response and data like:
```
{"error":"You must provided the parameter `lon`"}
```

Server errors will respond with 500 response and data like:
```
{"error":"A server error occured"}
```

