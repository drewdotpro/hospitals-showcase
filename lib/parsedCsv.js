'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _csvParse = require('csv-parse');

var _csvParse2 = _interopRequireDefault(_csvParse);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _util = require('util');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const parse = (0, _util.promisify)(_csvParse2.default);
const readFile = (0, _util.promisify)(_fs2.default.readFile);

class ParsedCSV {
  constructor(options = {}) {
    this.options = _lodash2.default.defaultsDeep({}, options, _config2.default);
  }

  async parse() {
    this.stringData = await readFile(this.options.file, this.options.encoding);
    this.parsedData = await parse(this.stringData, this.options.csvOptions);
    return this.parsedData;
  }
}

exports.default = ParsedCSV;