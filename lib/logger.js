'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _winston = require('winston');

var _winston2 = _interopRequireDefault(_winston);

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_winston2.default.configure({
  transports: [new _winston2.default.transports.Console({
    json: false,
    colorize: true,
    level: _config2.default.logLevel
  })]
});

exports.default = _winston2.default;