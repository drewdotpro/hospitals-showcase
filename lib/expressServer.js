'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _helmet = require('helmet');

var _helmet2 = _interopRequireDefault(_helmet);

var _compression = require('compression');

var _compression2 = _interopRequireDefault(_compression);

var _winston = require('winston');

var _winston2 = _interopRequireDefault(_winston);

var _expressWinston = require('express-winston');

var _expressWinston2 = _interopRequireDefault(_expressWinston);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class ExpressServer {
  constructor(geoSearch, options = {}) {
    this.options = _lodash2.default.defaultsDeep({}, options, _config2.default);
    this.geoSearch = geoSearch;
    this.app = (0, _express2.default)();
    this.app.use(_expressWinston2.default.logger({
      transports: [new _winston2.default.transports.Console({
        json: false,
        colorize: true,
        level: this.options.logLevel
      })],
      meta: true,
      msg: 'HTTP {{req.method}} {{req.url}}',
      expressFormat: true,
      colorize: true
    }));
    this.app.use((0, _helmet2.default)());
    this.app.use((0, _compression2.default)());
    this.app.get('/nearest', this.nearest.bind(this));
    this.errorHandler();
    _logger2.default.debug('ExpressServer created');
  }

  listen() {
    return new Promise((resolve, reject) => {
      try {
        this.app.listen(this.options.port, () => {
          _logger2.default.info(`Listening on port ${this.options.port}`);
          resolve();
        });
      } catch (e) {
        reject(e);
      }
    });
  }

  nearest(req, res, next) {
    const { lat, lon } = req.query;
    _logger2.default.debug(`Recevied request for nearest with lat '${lat}' and lon '${lon}'`);
    try {
      (0, _assert2.default)(lat, 'You must provided the parameter `lat`');
      (0, _assert2.default)(lon, 'You must provided the parameter `lon`');
      (0, _assert2.default)(!Number.isNaN(parseFloat(lat)), 'lat is NaN');
      (0, _assert2.default)(!Number.isNaN(parseFloat(lon)), 'lon is NaN');
    } catch (e) {
      e.status = 422;
      return next(e);
    }
    try {
      const results = this.geoSearch.nearest(lat, lon);
      return res.json(results);
    } catch (e) {
      return next(e);
    }
  }

  errorHandler() {
    const handler = (err, req, res, next) => {
      if (res.headersSent) {
        return next(err);
      }
      const status = err.status || 500;
      res.status(status);
      let errorMessage = 'A server error occured';
      if (status >= 400 && status < 500) {
        errorMessage = err.message;
        _logger2.default.debug(err);
      } else {
        _logger2.default.error(err);
      }
      const result = {
        error: errorMessage
      };
      return res.json(result);
    };
    this.app.use(handler);
  }
}

exports.default = ExpressServer;