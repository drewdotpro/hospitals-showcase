'use strict';

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

var _parsedCsv = require('./parsedCsv');

var _parsedCsv2 = _interopRequireDefault(_parsedCsv);

var _geoSearch = require('./geoSearch');

var _geoSearch2 = _interopRequireDefault(_geoSearch);

var _expressServer = require('./expressServer');

var _expressServer2 = _interopRequireDefault(_expressServer);

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const parser = new _parsedCsv2.default();
const start = async () => {
  _logger2.default.debug(`Starting with default config: ${JSON.stringify(_config2.default)}`);
  const data = await parser.parse();
  const geoSearch = new _geoSearch2.default(data);
  const expressServer = new _expressServer2.default(geoSearch);
  return expressServer.listen();
};

start().then(() => {
  _logger2.default.debug('Server Started');
}).catch(e => {
  _logger2.default.error(e);
});