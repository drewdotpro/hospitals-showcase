'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _geoNearby = require('geo-nearby');

var _geoNearby2 = _interopRequireDefault(_geoNearby);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class GeoSearch {
  constructor(data, options = {}) {
    this.options = _lodash2.default.defaultsDeep({}, options, _config2.default);
    if (!data) {
      throw new Error('No data provided');
    }
    this.data = data;
    const compactSetData = this.data.map((hospital, idx) => [parseFloat(hospital.Latitude), parseFloat(hospital.Longitude), idx]);
    const dataSet = _geoNearby2.default.createCompactSet(compactSetData);
    this.geo = new _geoNearby2.default(dataSet, { sorted: true, limit: this.options.searchLimit });
    _logger2.default.debug('GeoSearch created');
  }

  nearest(lat, lon) {
    const results = this.geo.nearBy(parseFloat(lat), parseFloat(lon), [0, this.options.searchRadius]);
    _logger2.default.debug(`GeoSearch for ${lat}, ${lon}`, results);
    return results.map(({ i }) => {
      const {
        OrganisationID, OrganisationCode, OrganisationName, Latitude, Longitude
      } = this.data[i];
      return {
        organisationID: OrganisationID,
        organisationCode: OrganisationCode,
        organisationName: OrganisationName,
        Latitude: parseFloat(Latitude).toString(),
        Longitude: parseFloat(Longitude).toString()
      };
    });
  }
}

exports.default = GeoSearch;